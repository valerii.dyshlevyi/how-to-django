from django.db import models

from apps.accounts.models import CustomUser


class Depot(models.Model):
    name = models.CharField(max_length=50, unique=True)

    class Meta:
        verbose_name = "Depot"
        verbose_name_plural = "Depots"

    def __str__(self):
        return f"Depot #{self.pk}"


class Vehicle(models.Model):
    """Vehicle model."""

    make = models.CharField(max_length=30, db_index=True)
    model = models.CharField(max_length=30, db_index=True)
    plate_number = models.CharField(
        max_length=8, help_text="Use the following format: 'AA1111BP'", unique=True
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    depot = models.ForeignKey(
        Depot, on_delete=models.SET_NULL, null=True, blank=True, related_name="depot_vehicles"
    )
    allowed_drivers = models.ManyToManyField(
        CustomUser,
        related_name="driver_vehicles",
        limit_choices_to={"profile_type": CustomUser.ProfileType.DRIVER},
        null=True,
        blank=True,
    )
    year_of_production = models.IntegerField(default=2022)

    class Meta:
        verbose_name = "Vehicle"
        verbose_name_plural = "Vehicles"

    def __str__(self):
        return f"{self.make} {self.model}"
