from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.exceptions import NotFound
from rest_framework.permissions import AllowAny
from rest_framework.views import Response, status
from rest_framework.viewsets import ModelViewSet

from apps.vehicles.api.serializers import VehicleSerializer
from apps.vehicles.models import Vehicle


###############################################################################################
class VehiclesList(generics.ListCreateAPIView):
    queryset = Vehicle.objects.select_related("depot").all()
    serializer_class = VehicleSerializer
    permission_classes = [AllowAny]


# Or like this


@api_view(["GET", "POST"])
def vehicles_list(request):
    if request.method == "GET":
        vehicles = Vehicle.objects.all().select_related("depot")
        return Response(data=VehicleSerializer(vehicles, many=True).data, status=status.HTTP_200_OK)

    serializer = VehicleSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    new_vehicle = serializer.save()
    return Response(data=VehicleSerializer(new_vehicle).data)


###############################################################################################
###############################################################################################
class VehicleRetrieveUpdateDelete(generics.RetrieveUpdateDestroyAPIView):
    queryset = Vehicle.objects.select_related("depot").all()
    serializer_class = VehicleSerializer
    permission_classes = [AllowAny]


# Or like this


@api_view(["GET", "PATCH", "DELETE"])
def vehicle_details(request, *args, **kwargs):
    vehicle = Vehicle.objects.filter(id=kwargs.get("pk")).first()
    if not vehicle:
        raise NotFound
    if request.method == "GET":
        return Response(data=VehicleSerializer(vehicle).data, status=status.HTTP_200_OK)
    if request.method == "PATCH":
        serializer = VehicleSerializer(data=request.data, instance=vehicle, context={"sender": request.user}, partial=True)
        serializer.is_valid(raise_exception=True)
        new_vehicle = serializer.save()
        return Response(data=VehicleSerializer(new_vehicle).data)
    vehicle.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)


###############################################################################################


class VehicleViewSet(ModelViewSet):
    """
    A simple ViewSet for viewing and editing accounts.
    """

    queryset = Vehicle.objects.all().select_related("depot")
    serializer_class = VehicleSerializer
    permission_classes = [AllowAny]
