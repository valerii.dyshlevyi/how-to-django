import datetime

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from apps.vehicles.models import Depot, Vehicle


class DepotSerializer(serializers.ModelSerializer):
    class Meta:
        model = Depot
        fields = ("id", "name")


class VehicleSerializer(serializers.ModelSerializer):
    # depot_details = DepotSerializer(read_only=True, source="depot")
    depot_details = serializers.SerializerMethodField()
    sent_to_nasa = serializers.SerializerMethodField()

    class Meta:
        model = Vehicle
        fields = (
            "id",
            "make",
            "model",
            "plate_number",
            "year_of_production",
            "depot",
            "depot_details",
            "sent_to_nasa",
        )

    @staticmethod
    def validate_make(make):
        if make not in ("Toyota", "Ford", "Renault"):
            raise ValidationError("Unknown make.")
        return make

    def validate(self, attrs):
        sender = self.context.get("sender")
        # make = attrs.get("make")
        # if make not in ("Toyota", "Ford", "Renault"):
        #     raise ValidationError("Unknown make.")
        return attrs

    @staticmethod
    def get_sent_to_nasa(obj):
        return "Sent to NASA"

    @staticmethod
    def get_depot_details(obj):
        return DepotSerializer(obj.depot).data if obj.depot else {}


def return_current_date():
    return datetime.datetime.now()
