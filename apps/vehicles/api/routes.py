# from django.urls import path
from os import path

from . import views

app_name = "vehicles"

from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r"vehicles", views.VehicleViewSet, basename="vehicle",)

urlpatterns = [
    # path("vehicles_list/", views.vehicles_list, name="vehicle_list"),
    # path("vehicles_list/", views.VehiclesList.as_view(), name="vehicle_list"),
    # path("vehicles_list/<int:pk>/", views.vehicle_details, name="vehicle_details"),
] + router.urls
