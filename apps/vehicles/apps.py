from django.apps import AppConfig


class VehiclesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "apps.vehicles"

    def ready(self):
        # Implicitly connect a signal handlers decorated with @receiver.
        pass

        # Explicitly connect a signal handler.
        # request_finished.connect(signals.pre_save_vehicle)
        # request_finished.connect(signals.post_save_vehicle)
