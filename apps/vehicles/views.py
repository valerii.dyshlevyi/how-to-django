import datetime

from django.http import HttpResponse
from django.views.generic import ListView
from django.views.generic.edit import UpdateView
from rest_framework import viewsets

from .models import Vehicle


# def current_datetime(request):
#     now = datetime.datetime.now()
#     html = "<html><body>It is now %s.</body></html>" % now
#     return HttpResponse(html)
#
#
# class VehicleListView(ListView):
#     model = Vehicle
#     template_name = "vehicle_list.html"
#     paginate_by = 10
#
#
# class VehicleUpdateView(UpdateView):
#     model = Vehicle
#     template_name = "vehicle_update.html"
#     fields = ("model", "make")
#     success_url = "/"


# class VehicleViewSet(viewsets.ModelViewSet):
