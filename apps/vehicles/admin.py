from django.contrib import admin

from apps.vehicles.models import Depot, Vehicle


@admin.register(Vehicle)
class VehicleAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "make",
        "model",
        "plate_number",
        "depot",
    )
    list_filter = ("make",)
    search_fields = ("make", "model")
    raw_id_fields = ("depot",)
    list_select_related = ("depot",)


@admin.register(Depot)
class DepotAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "name",
    )
    search_fields = ("name",)
