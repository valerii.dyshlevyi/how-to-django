import datetime

from django.urls import reverse
from django.test import override_settings
from rest_framework import status
from rest_framework.test import APITestCase

from apps.vehicles.models import Vehicle
from apps.vehicles.api.serializers import VehicleSerializer, return_current_date

from unittest.mock import patch


def mock_sent_to_nasa(obj):
    return f"{obj.make} {obj.model}"


class VehicleTestCase(APITestCase):
    fixtures = ["account.json", "vehicle.json", ]

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # cls.vehicle = Vehicle.objects.create()
        print("setUpClass")

    def setUp(self):
        self.vehicle = Vehicle.objects.create(make="Nissan", model="Leaf", plate_number="AE3466BP")
        print("setUp")

    @classmethod
    def tearDownClass(cls):
        print("tearDownClass")
        super().tearDownClass()

    def tearDown(self):
        print("tearDown")

    def test_vehicle_get(self):
        print("test_vehicle_get")
        url = reverse("vehicles:vehicle-list")
        response = self.client.get(url)
        print(response.json())
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = reverse("vehicles:vehicle-detail", args=(self.vehicle.pk,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.json()
        print(data)
        self.assertEqual(data.get("plate_number"), self.vehicle.plate_number)

    def test_vehicle_post(self, mock_google, mock_tiktok, *_):
        print("test_vehicle_post")

        url = reverse("vehicles:vehicle-list")

        # Negative test case with no correct make
        new_vehicle_data = {
            "make": "Subaru",
            "model": "Impreza",
            "plate_number": "AE2345CX",
            "year_of_production": 2018
        }
        response = self.client.post(url, data=new_vehicle_data,)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # Positive test case with correct data
        new_vehicle_data["make"] = "Toyota"
        new_vehicle_data["model"] = "Karina"

        response = self.client.post(url, data=new_vehicle_data)
        data = response.json()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(data.get("sent_to_nasa"), "Sent to NASA")

        # with patch.object(VehicleSerializer, "get_sent_to_nasa", return_value="Do not sent to NASA"):
        #     new_vehicle_data["plate_number"] = "AE2346CX"
        #     response = self.client.post(url, data=new_vehicle_data)
        #     data = response.json()
        #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        #     self.assertEqual(data.get("sent_to_nasa"), "Do not sent to NASA")

        with patch.object(VehicleSerializer, "get_sent_to_nasa", side_effect=mock_sent_to_nasa) as _mock:
            new_vehicle_data["plate_number"] = "AE2346CX"
            response = self.client.post(url, data=new_vehicle_data)
            data = response.json()
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertEqual(data.get("sent_to_nasa"), f"{new_vehicle_data.get('make')} {new_vehicle_data.get('model')}")

            new_vehicle_data["plate_number"] = "AE2347CX"
            response = self.client.post(url, data=new_vehicle_data)
            data = response.json()
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertEqual(data.get("sent_to_nasa"),
                             f"{new_vehicle_data.get('make')} {new_vehicle_data.get('model')}")

            self.assertTrue(_mock.called)
            self.assertEqual(_mock.call_count, 1)

    @patch("apps.vehicles.api.serializers.return_current_date", datetime.datetime(2022, 2, 23, 9, 5, 0))
    def test_current_date(self,):
        self.assertEqual(return_current_date().day, datetime.datetime.now().day)

        with patch("apps.vehicles.tests.return_current_date", return_value=datetime.datetime(2022, 2, 23, 9, 5, 0)):
            self.assertEqual(return_current_date(), datetime.datetime(2022, 2, 23, 9, 5, 0))

