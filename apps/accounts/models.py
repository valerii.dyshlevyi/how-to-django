from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.utils.translation import gettext_lazy as _


class UserManager(BaseUserManager):
    def create_user(self, username, email, password=None, **other):
        user = self.model(email=self.normalize_email(email), username=username, **other)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, password):
        user = self.create_user(
            first_name="Admin",
            last_name="Adminski",
            username=username,
            email=email,
            password=password,
            is_staff=True,
            is_superuser=True,
            profile_type=0,
        )
        return user


class CustomUser(AbstractUser):
    class ProfileType(models.TextChoices):
        STAFF = _("Staff")
        DRIVER = _("Driver")

    profile_type = models.CharField(
        choices=ProfileType.choices, default=ProfileType.STAFF, max_length=50
    )

    is_online = models.BooleanField(default=False)

    objects = UserManager()

    class Meta:
        verbose_name = _("User")
        verbose_name_plural = _("Users")
